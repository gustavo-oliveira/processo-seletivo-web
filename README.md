# processo-seletivo-web
O projeto é apenas uma página HTML de um Crud em Vue.js e Axios porém foi tirada toda a complexidade do projeto e deixamos apenas 1 página consumindo os serviços.

É preciso que você ajeite o layout do formulário baseado no Manual de Identidade Visual que está na raiz do projeto (HEPTA DIGITAL.PDF).


# Requisitos

1. Front-end usando HTML, CSS e Javascript

# O que fazer agora

Faça um clone desse projeto e ao terminar envie o link do projeto no seu repositório para gustavo.oliveira@hepta.com.br com o título "Processo seletivo - [seu nome]", se você nunca usou git crie uma conta no gitlab e 
dê uma olhada nos links abaixo.

# Avaliação

O objetivo desse teste é medir o seu conhecimento sobre as boas práticas de design, criatividade e formatação.


# Links úteis

* Git
    * [git - guia prático](http://rogerdudler.github.io/git-guide/index.pt_BR.html)
    * [git - documentacao](https://git-scm.com/book/pt-pt/v2)


* HTML/CSS e Javascript 
    * [Tudo sobre Javascript, HTML e CSS](https://www.w3schools.com/whatis/) 
    * [HTML](https://www.w3schools.com/html/default.asp)
    * [CSS](https://www.w3schools.com/css/default.asp)
    * [Javascript](https://www.w3schools.com/js/default.asp)
    * [bootstrap](https://getbootstrap.com/)
    * [W3 Bootstrap](https://www.w3schools.com/bootstrap/default.asp)
    * [Vue JS](https://vuejs.org/) - Uma lib JS baseada em Angular porém mais simples.
    * [Axios](https://vuejs.org/v2/cookbook/using-axios-to-consume-apis.html) - Lib JS recomendada para Vue JS para simplificar XMLHttpRequests.


